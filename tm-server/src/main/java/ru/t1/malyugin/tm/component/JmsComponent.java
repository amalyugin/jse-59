package ru.t1.malyugin.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;

import javax.jms.*;

@Component
public class JmsComponent {

    @Getter
    private final MessageProducer messageProducer;

    @Getter
    private final Session session;

    @Autowired
    public JmsComponent(
            @NotNull final IPropertyService propertyService,
            @NotNull final ConnectionFactory connectionFactory
    ) throws JMSException {
        @NotNull final String queueName = propertyService.getJmsQueueName();
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(queueName);
        messageProducer = session.createProducer(destination);
    }

}