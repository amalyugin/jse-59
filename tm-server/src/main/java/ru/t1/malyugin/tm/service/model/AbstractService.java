package ru.t1.malyugin.tm.service.model;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.malyugin.tm.api.repository.model.IRepository;
import ru.t1.malyugin.tm.api.service.model.IService;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.List;

public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected abstract IRepository<M> getRepository();

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        getRepository().add(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        getRepository().remove(model);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        getRepository().update(model);
    }

    @Override
    public long getSize() {
        return getRepository().getSize();
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().clear();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return getRepository().findOneById(id.trim());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(id);
        if (model == null) return;
        remove(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return getRepository().findAll();
    }

}