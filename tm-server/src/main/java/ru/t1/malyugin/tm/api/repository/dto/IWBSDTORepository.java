package ru.t1.malyugin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.model.AbstractWBSDTOModel;

import java.util.List;

public interface IWBSDTORepository<M extends AbstractWBSDTOModel> extends IUserOwnedDTORepository<M> {

    @NotNull
    List<M> findAllOrderByCreated(@NotNull String userId);

    @NotNull
    List<M> findAllOrderByName(@NotNull String userId);

    @NotNull
    List<M> findAllOrderByStatus(@NotNull String userId);

}
