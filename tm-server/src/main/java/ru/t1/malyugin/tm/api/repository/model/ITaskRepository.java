package ru.t1.malyugin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IWBSRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

}